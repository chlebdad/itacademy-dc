package com.itacademy.hw_collection;

import java.util.ArrayList;
import java.util.List;

public class Person {

    /*

Homework:
Create Person class with three fields: int age, String name, List<Person> relatives
Create at least three instances of Person class: “mother”, “father” and “child” in the following way:
“mother” instance should have random “age” and “name” but “relatives” list should contain “father” and “child” instance
“father” and “child” should have also random names and ages but relatives should be populated accordingly

     */
    int age;
    String name;
    List<Person> relatives = new ArrayList<>();

    public Person(String name, int age) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getRelatives() {
        return relatives;
    }

    public void setRelatives(List<Person> relatives) {
        this.relatives = relatives;
    }

    @Override
    public String toString() {
        return getName();
    }
}
