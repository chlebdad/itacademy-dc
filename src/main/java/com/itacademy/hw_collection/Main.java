package com.itacademy.hw_collection;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        Person mother = new Person("Maria", 27);
        Person father = new Person("Jacek", 30);
        Person child = new Person("Monika", 5);

        List<Person> motherRelatives = List.of(father, child);
        List<Person> fatherRelatives = List.of(mother, child);
        List<Person> childRelatives = List.of(father, mother);

        mother.setRelatives(motherRelatives);
        father.setRelatives(fatherRelatives);
        child.setRelatives(childRelatives);

        System.out.println("Mother relatives: " + motherRelatives.toString());
    }
}
