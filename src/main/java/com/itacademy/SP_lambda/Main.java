package com.itacademy.SP_lambda;

import java.util.*;

public class Main {

    List<String> list = new ArrayList();
    private int flag = 0;

    Scanner scanner = new Scanner(System.in);


    private List<String> sortList() {

        while (flag != 5) {
            list.add(scanner.nextLine());
            flag++;
        }
        flag = 0;

        list.sort(Comparator.naturalOrder());

        list.sort(Comparator.comparing(String::length).reversed());
        list.forEach(System.out::println);

        return list;
    }

}