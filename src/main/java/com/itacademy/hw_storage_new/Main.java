package com.itacademy.hw_storage_new;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        ProcessData data = new ProcessData();
        data.getState("ProductA");
        data.modifyState("set", "ProductA", null);
        data.modifyState("add", "ProductA", 0);
        data.getState("ProductA");
        data.exportState();
        data.getState("ProductB");
        data.modifyState("add", "ProductB", 123);
        data.getState("ProductB");
        data.importState();
        data.getState("ProductB");
        data.printState();

    }
}
