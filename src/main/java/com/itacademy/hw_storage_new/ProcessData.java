package com.itacademy.hw_storage_new;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class ProcessData {

    private static final String EXPORT_FILE_NAME = "dataExport.json";
    static HashMap<String, Integer> listOfProducts = new HashMap<>();
    private static final ObjectMapper mapper = new ObjectMapper();

    ProcessData() throws IOException {
        new OpenData().openData();
    }

    void getState(String productName) {

        if (listOfProducts.containsKey(productName)) {
            System.out.println("Storage state: " + productName + " quantity: " + listOfProducts.get(productName));
        } else {
            System.out.println("Searched product: " + productName + " not found in database!");
        }

    }

    static void modifyState(String type, String productName, Integer productQuantity) {

        if (productName != null && productQuantity != null) {

            if (type.equalsIgnoreCase("ADD")) {
                Commands.ADD.perform(productName, productQuantity);
            } else if (type.equalsIgnoreCase("REMOVE")) {
                Commands.REMOVE.perform(productName, productQuantity);
            } else if (type.equalsIgnoreCase("SET")) {
                Commands.SET.perform(productName, productQuantity);
            } else {
                System.out.println("Incorrect command - no changes applied.");
            }

        } else {
            System.out.println("Cannot modify the product state - improper input data");
        }

    }

    void exportState() {
        try {
            ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
            writer.writeValue(new File(OpenData.PATH + EXPORT_FILE_NAME), listOfProducts);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Export successful");
    }

    void importState() {

        try {
            TypeReference<HashMap<String, Integer>> typeReference = new TypeReference<>() {
            };
            Map<String, Integer> tempMap = mapper.readValue(new File(OpenData.PATH + EXPORT_FILE_NAME), typeReference);
            ProcessData.listOfProducts.clear();
            tempMap.forEach(Commands.SET::perform);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Import successful");
    }

    void printState() {
        try {
            String print = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(listOfProducts);
            System.out.println("Warehouse state:");
            System.out.println(print);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}
