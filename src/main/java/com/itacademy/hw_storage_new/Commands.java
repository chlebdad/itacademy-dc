package com.itacademy.hw_storage_new;

import java.util.Properties;

enum Commands {

    ADD {
        @Override
        void perform(String productName, Integer productQuantity) {

            if (!ProcessData.listOfProducts.containsKey(productName)) {
                ProcessData.listOfProducts.put(productName, productQuantity);
            } else {
                ProcessData.listOfProducts.compute(productName, (k, v) -> v + productQuantity);
            }
        }
    },
    REMOVE {
        @Override
        void perform(String productName, Integer productQuantity) {

            if (!ProcessData.listOfProducts.containsKey(productName)) {
                System.out.println("No such product: " + productName);
            } else {
                if (ProcessData.listOfProducts.get(productName) >= productQuantity) {
                    ProcessData.listOfProducts.compute(productName, (k, v) -> v + productQuantity);
                } else {
                    System.out.println("Cannot do that - given quantity: " + productQuantity + " extends storage state: " + ProcessData.listOfProducts.get(productName));
                }
            }
        }
    },
    SET {
        @Override
        void perform(String productName, Integer productQuantity) {
            ProcessData.listOfProducts.remove(productName);
            ProcessData.listOfProducts.put(productName, productQuantity);
        }
    };

    abstract void perform(String productName, Integer productQuantity);
}

