package com.itacademy.hw_storage_new;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

class OpenData {

    static final String PATH = "src/main/resources/";

    void openData() throws IOException {

        Path dataPath = FileSystems.getDefault().getPath(PATH + "InputData.csv");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(dataPath.toFile()))) {

            String line;
            while ((line = bufferedReader.readLine()) != null) {

                String[] splittedEntry = line.split(";");

                if (!splittedEntry[3].isEmpty()) {
                    for (int i = 3; i < splittedEntry.length; i++) {
                        String[] splittedProducts = splittedEntry[i].split(":");
                        ProcessData.modifyState(splittedEntry[1], splittedProducts[0], Integer.parseInt(splittedProducts[1]));
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
