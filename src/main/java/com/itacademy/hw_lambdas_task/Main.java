package com.itacademy.hw_lambdas_task;

import java.util.function.Consumer;
import java.util.function.Function;

public class Main {
    private static final String CONSUMER_INPUT = "consumer input";
    private static final Person PERSON = new Person("John", 34);
    private static final String MESSAGE = "message";

    public static void main(String[] args) {
        // TODO create lambda body (whatever you like) for every method below

       Consumer<String> printmsg = sth -> System.out.println(sth);
       Function<Person, Integer> fun = sth -> sth.getAge();
       ISender mailsend = sth -> {return;};
       ISender lettersend = sth -> {return;};

    }

    void consumer(Consumer<String> printer) {
        printer.accept(CONSUMER_INPUT);
    }

    int function(Function<Person, Integer> calculateAge) {
        return calculateAge.apply(PERSON);
    }

    void sendMail(ISender mailSender) {
        mailSender.send(MESSAGE);
    }

    void sendLetter(ISender letterSender) {
        letterSender.send(MESSAGE);
    }

    interface ISender {
        void send(String message);
    }


}
