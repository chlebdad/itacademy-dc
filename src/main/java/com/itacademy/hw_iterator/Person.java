package com.itacademy.hw_iterator;

import java.util.*;

public class Person implements Comparable<Person>{

    String name;
    String surname;
    int age;
    Set<Person> family = new HashSet<>();

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        //this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Person> getFamilyMembers() {
        List<Person> sortedfamily = new ArrayList<>(family);
       // sortedfamily.addAll(family);
        Collections.sort(sortedfamily, Collections.reverseOrder());
        return sortedfamily;
    }

    @Override
    public int compareTo(Person member) {
        Integer newAge = Integer.valueOf(member.age);
        Integer secondAge = Integer.valueOf(this.age);
        return secondAge.compareTo(newAge);
    }

    void addFamilyMember(Person member){


        family.add(member);
        System.out.println(member + " has been added to the Family");

    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", family=" + family +
                '}';
    }
}
