package com.itacademy.hw_iterator;

import java.util.ArrayList;
import java.util.List;

public class Main {

    /*
    TASK:
Create "Person" class with fields:
String name, String surname, int age, Set<Person> family
create setters and getters
create method "void addFamilyMember(Person member)" which adds “member” to "family" set
create "getFamilyMembers" method which sorts (descending order) the "family" set based on "age" and returns collection
Can I return set if I need sorted collection?
TIP: use Collections.sort
TIP2: use Comparator or implement Comparable interface

Create Main class with main method
create several people of Person class (should have some duplicated “surnames”)
add all of them to the "List people"
for each person in “people” list
iterate rest of a people and
if any instance in people list has the same “surname”
add it to "family" set for particular person
on every person execute "getFamilyMembers" and print it out
TIP: implement toString() method in Person class

Example:
Given: John Smith 45, Alice Smith 43, John Doe 34, Joshua Smith 20

John Smith should have: Alice Smith and Joshua Smith in “family” set
Alice Smith should have: John Smith and Joshua Smith in “family” set
Joshua Smith should have: John Smith and Alice Smith in “family” set
John Doe should have empty “family” set
     */

    public static void main(String[] args) {

    Person member = new Person("John", "Smith", 45);
    Person member2 = new Person("Alice", "Smith", 43);
    Person member3 = new Person("John", "Doe", 34);
    Person member4 = new Person("Joshua", "Smith", 20);

    List<Person> people = new ArrayList<>();
    member.addFamilyMember(member2);
    member.addFamilyMember(member3);
    member.addFamilyMember(member4);

    people.add(member);
    people.add(member2);
    people.add(member3);
    people.add(member4);


//        for (Iterator<Person> it = people.iterator(); it.hasNext(); ) {
//            Person value = it.next();
//            if(){
//
//            }
//        }




    }


}
