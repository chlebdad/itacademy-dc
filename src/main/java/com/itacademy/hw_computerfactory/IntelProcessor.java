package com.itacademy.hw_computerfactory;

public class IntelProcessor extends Processor {

    private String model;
    private  String manufacturer;

    public IntelProcessor() {
        this.model = "Xeon 6134";
        this.manufacturer = "Intel Corporation";
    }

    @Override
    int getMHz() {
        return 3000;
    }

    @Override
    public String toString() {
        return "processor model " + model + " produced by " + manufacturer;
    }
}
