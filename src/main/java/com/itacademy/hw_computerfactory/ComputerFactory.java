package com.itacademy.hw_computerfactory;

public class ComputerFactory {

    private Processor proc;
    private Gpu gpu;

    public ComputerFactory(Processor proc, Gpu gpu) {
        this.proc = proc;
        this.gpu = gpu;
    }

    void installProcessor(Processor proc){

        System.out.println("Chosen processor " + getProcessor() + " has been installed");

    }
    Processor getProcessor(){

        return this.proc;
    }
    void installGpu(Gpu gpu){

        System.out.println("Chosen gpu " + getGpu() + " has been installed");

    }
    Gpu getGpu(){

        return this.gpu;
    }

    @Override
    public String toString() {
        return "Your computer has been completed with:" +
                " " + proc +
                " and "+ gpu;
    }


}
