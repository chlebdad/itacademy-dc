package com.itacademy.hw_computerfactory;

public class AmdProcessor extends Processor {

    private String model;
    private String manufacturer;

    public AmdProcessor() {
        this.model = "Ryzen 5";
        this.manufacturer = "AMD Corporation";
    }

    @Override
    int getMHz() {
        return 2000;
    }


}
