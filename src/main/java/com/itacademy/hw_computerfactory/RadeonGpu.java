package com.itacademy.hw_computerfactory;

public class RadeonGpu extends Gpu {

    private String model;
    private String manufacturer;

    public RadeonGpu() {
        this.model = "RX 590";
        this.manufacturer = "Radeon Corporation";
    }

    @Override
    int getG3dMark() {
        return 180;
    }

    @Override
    public String toString() {
       return "gpu model " + model + " produced by " + manufacturer;
    }
}
