package com.itacademy.hw_computerfactory;

public class NVidiaGpu extends Gpu {

    private String model;
    private String manufacturer;

    public NVidiaGpu() {
        this.model = "GTX9500";
        this.manufacturer = "NVidia Corporation";
    }

    @Override
    int getG3dMark() {
        return 200;
    }

    @Override
    public String toString() {
        return "gpu model " + model + " produced by " + manufacturer;
    }
}
