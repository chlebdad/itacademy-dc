package com.itacademy.hw_computerfactory;

public class Main {

    public static void main(String[] args) {

        AmdProcessor amdProcessor = new AmdProcessor();
        IntelProcessor intelProcessor = new IntelProcessor();
        NVidiaGpu nVidiaGpu = new NVidiaGpu();
        RadeonGpu radeonGpu = new RadeonGpu();

        ComputerFactory pc1 = new ComputerFactory(amdProcessor, radeonGpu);
        ComputerFactory pc2 = new ComputerFactory(intelProcessor, nVidiaGpu);

        pc1.installGpu(nVidiaGpu);
        System.out.println(pc1.toString());

        pc2.installProcessor(intelProcessor);
        System.out.println(pc2.toString());

    }
}
