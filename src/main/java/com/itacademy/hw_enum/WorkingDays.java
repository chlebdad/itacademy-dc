package com.itacademy.hw_enum;

import java.util.List;

public interface WorkingDays {

    List<Day> getDays();
}
