package com.itacademy.hw_enum;

public enum Day {

        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY{

            @Override
            public float calculateDailyPay(int basicDailyPay) {

                return 1.5F*basicDailyPay;

            }
        },
        SUNDAY{
            @Override
            public float calculateDailyPay(int basicDailyPay) {

                return 2.0F*basicDailyPay;

            }

        };

    float payFactor = 1.0F;

    public float calculateDailyPay(int basicDailyPay){
        return payFactor*basicDailyPay;
    }
}
