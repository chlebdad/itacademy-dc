package com.itacademy.hw_enum;

import java.util.Arrays;

public class Person {

    private String name;
    private WorkingDays workingDays;
    private int basicDailyPay;

    public Person(String name, WorkingDays workingDays, int basicDailyPay) {
        this.name = name;
        this.workingDays = workingDays;
        this.basicDailyPay = basicDailyPay;
    }

    float calculateWeeklySalary(){

        float salary = 0.0F;

        for (Day d: workingDays.getDays()) {
            salary += d.calculateDailyPay(basicDailyPay);
        }
        return salary;
    }

    public String getName() {
        return name;
    }


    public WorkingDays getWorkingDays() {
        return workingDays;
    }

    public int getBasicDailyPay() {
        return basicDailyPay;
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + getName() + '\'' +
                ", workingDays=" + Arrays.toString(getWorkingDays().getDays().toArray()) +
                ", basicDailyPay=" + getBasicDailyPay() +
                '}';
    }
}
