package com.itacademy.hw_enum;

import java.util.ArrayList;
import java.util.List;

public class Main {

    static WorkingDays workingdays = () -> {

        List<Day> list = new ArrayList<>();
        list.add(Day.FRIDAY);
        list.add(Day.MONDAY);

        return list;
    };

    static class EmergencyWorkingDays implements WorkingDays {


        @Override
        public List<Day> getDays() {

            List<Day> list = new ArrayList<>();
            list.add(Day.SUNDAY);
            list.add(Day.SATURDAY);

            return list;
        }
    }


    public static void main(String[] args) {

        Person dominika = new Person("Dominika", new EmergencyWorkingDays(), 100);
        Person karol = new Person("Karol", workingdays, 300);

        System.out.println(dominika.calculateWeeklySalary());
        System.out.println(dominika.toString());

        System.out.println(karol.calculateWeeklySalary());
        System.out.println(karol.toString());


    }


}
