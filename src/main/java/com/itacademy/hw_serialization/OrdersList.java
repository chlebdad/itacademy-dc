package com.itacademy.hw_serialization;

import java.util.ArrayList;
import java.util.List;

public class OrdersList {

    List<Order> orders = new ArrayList<>();

    public OrdersList(List<Order> orders) {
        this.orders = orders;
    }

    public OrdersList(){
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "OrdersList{" +
                "orders=" + orders +
                '}';
    }
}
