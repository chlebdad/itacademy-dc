package com.itacademy.hw_serialization;

public class Customer {

    private int cust_id;
    private String first_name;
    private String last_name;
    private String city;

    public Customer(int cust_id, String first_name, String last_name, String city) {
        this.cust_id = cust_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.city = city;
    }

    public Customer(){

    }

    public int getCust_id() {
        return cust_id;
    }

    public void setCust_id(int cust_id) {
        this.cust_id = cust_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "cust_id=" + cust_id +
                ", firstname='" + first_name + '\'' +
                ", lastname='" + last_name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
