package com.itacademy.hw_serialization;

public class Order {

    private int order_no;
    private  String date;
    private Customer customer;

    public Order(int no, String date, Customer customer) {
        this.order_no = order_no;
        this.date = date;
        this.customer = customer;
    }

    public Order(){}

    public int getOrder_no() {
        return order_no;
    }

    public void setOrder_no(int order_no) {
        this.order_no = order_no;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Order{" +
                "order_int=" + order_no +
                ", date='" + date + '\'' +
                ", customer=" + customer +
                '}';
    }
}

