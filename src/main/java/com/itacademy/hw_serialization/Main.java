package com.itacademy.hw_serialization;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        OrdersList orderWithCustomer = new ObjectMapper().readValue(new File("src/main/resources/test.json"), OrdersList.class);
        System.out.println(orderWithCustomer);

    }
}


