package com.itacademy.hw_storage_old;

import java.util.HashMap;
import java.util.Map;

public class StorageData {

    private String operation;
    private String timestamp;
    private Map<String, Integer> list;
    static HashMap<String, Integer> listOfProducts = new HashMap<>();

    StorageData(String operation, String timestamp, Map<String, Integer> list) {
        this.operation = operation;
        this.timestamp = timestamp;
        this.list = list;
    }

    @Override
    public String toString() {
        return "StorageData{" +
                "operation='" + operation + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", listofProducts=" + listOfProducts +
                '}';
    }

    //    <nr_of_entry> is the integer.
//    <operation> can be one of two "ADD" or "REMOVE". Add to existing or remove from.
//    <timestamp> is the date-time of the operation.
//    <list_of_products> specifies the pattern that can be repeated, e.g. <product_id>:<count>;<product_id>:<count>;<product_id>:<count>
}
