package com.itacademy.hw_storage_old;

public interface Database {

    void getState(String productName);
    void modifyState(String type, String productName, Integer productQuantity);

}
