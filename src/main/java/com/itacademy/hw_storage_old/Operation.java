package com.itacademy.hw_storage_old;

enum Operation {

    ADD {
        @Override
        void perform(String productName, Integer productQuantity) {

            if (!StorageData.listOfProducts.containsKey(productName)) {
                StorageData.listOfProducts.put(productName, productQuantity);
            } else {
                StorageData.listOfProducts.computeIfPresent(productName, (k, v) -> v + productQuantity);
            }
        }
    },
    REMOVE {
        @Override
        void perform(String productName, Integer productQuantity) {

            if (!StorageData.listOfProducts.containsKey(productName)) {
                System.out.println("No such product: " + productName);
            } else {
                if (StorageData.listOfProducts.get(productName) >= productQuantity) {
                    StorageData.listOfProducts.computeIfPresent(productName, (k, v) -> v + productQuantity);
                } else {
                    System.out.println("Cannot do that - given quantity: " + productQuantity + " extends storage state: " + StorageData.listOfProducts.get(productName));
                }

            }
        }
    };

    abstract void perform(String productName, Integer productQuantity);
}

