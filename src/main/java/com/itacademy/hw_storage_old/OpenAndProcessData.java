package com.itacademy.hw_storage_old;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class OpenAndProcessData implements Database {

    private static Map<Integer, StorageData> mapData = new HashMap<>();


    public void openData() throws IOException {

        BufferedReader bufferedReader = null;
        //String HOME = System.getProperty("user.home");

        try {
            Path dataPath = FileSystems.getDefault().getPath("src/main/resources/inputData.csv");
            bufferedReader = new BufferedReader(new FileReader(dataPath.toFile()));
            String line;

            HashMap archivedProductList = new HashMap<String, Integer>();

            while ((line = bufferedReader.readLine()) != null) {

                String[] splitted = line.split(";");

                if (!splitted[3].isEmpty()) {
                    for (int i = 3; i < splitted.length; i++) {
                        String[] splittedProducts = splitted[i].split(":");
                        modifyState(splitted[1], splittedProducts[0], Integer.parseInt(splittedProducts[1]));
                        archivedProductList.put(splittedProducts[0], splittedProducts[1]);
                    }
                }
                mapData.put(Integer.valueOf(splitted[0]), new StorageData(splitted[1], splitted[2], archivedProductList));

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } finally {
            bufferedReader.close();
        }

    }


    @Override
    public void getState(String productName) {

        if(StorageData.listOfProducts.containsKey(productName)){
            System.out.println("Storage state: " + productName + " quantity: " + StorageData.listOfProducts.get(productName));
        }else{
            System.out.println("Searched product: " + productName + " not found in database!");
        }

    }

    @Override
    public void modifyState(String type, String productName, Integer productQuantity) {

        if(productName != null && productQuantity !=null){

                if(type.equalsIgnoreCase("ADD")){
                    Operation.ADD.perform(productName, productQuantity);
                }else if(type.equalsIgnoreCase("REMOVE")){
                    Operation.REMOVE.perform(productName, productQuantity);
                }else {
                    System.out.println("Incorrect command - no changes applied.");
                }

        }else{
            System.out.println("Cannot modify the product state - improper input data");
        }

    }
}
