package com.itacademy.hw_storage_old;

import java.io.*;

public class Main {

    /*
    Task description

In general, we would like to have Storage that will keep the count of our products. The state change is performed based on a CSV file which consists of:
<nr_of_entry>;<operation>;<timestamp>;<list_of_products>

where:
<nr_of_entry> is the integer.
<operation> can be one of two "ADD" or "REMOVE". Add to existing or remove from.
<timestamp> is the date-time of the operation.
<list_of_products> specifies the pattern that can be repeated, e.g. <product_id>:<count>;<product_id>:<count>;<product_id>:<count>

Example of the file would look like:
1;ADD;2017-01-01T12:00:00.000Z;ProductA:120;ProductB:50;ProductC:100;
2;REMOVE;2017-01-01T14:00:00.000Z;ProductA:5;ProductC:20;
3;ADD;2017-01-01T15:00:00.000Z;ProductB:400

Storage exposes API that allows for getting actual state and modifying it.
    Get state:
        Creates a file that has one line with the actual count of each product, e.g. :
            ProductA:50;ProductB:150;ProductC:200;
    Modify state:
        - Takes Path to the CSV file processes it according to the description above

     */


    public static void main(String[] args) throws IOException {

        OpenAndProcessData openData = new OpenAndProcessData();
        openData.openData();
        openData.getState("ProductA");
        openData.modifyState("remove", "ProductA", 1000);
        openData.getState("ProductA");

    }
}
