package com.itacademy.hw_streams;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    /*

    Given the List.of(“one”, “two”, “three”, “four”):
    Filter all words that consist of at least one “e” letter
    Map every word to camel-case version e.g “three” -> “tHrEe”
    Collect every stream to list

     */

    Stream<String> listStream = (Stream<String>) List.of("one", "two", "three", "four").stream()
            .filter(s -> s.contains("e"))
            .map(s -> s.toUpperCase())  // CaseUtils.toCamelCase => appache commons
            .collect(Collectors.toList());


}
