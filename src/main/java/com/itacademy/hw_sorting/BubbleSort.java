package com.itacademy.hw_sorting;

import java.util.Arrays;

public class BubbleSort {


    public static int[] sorting(int[] list){

        Arrays.sort(list);
        return list;
    }

    public static int[] sorting2(int[] list){


        for(int j=0; j<list.length-1; j++){
            for(int i=0; i<list.length-1; i++){
                if (list[i] > list[i+1]){
                    int temp = list[i+1];
                    list[i+1] = list[i];
                    list[i] = temp;
                }
            }
        }
        return list;
    }
    // + insertion sort wiki + geekforgeeks







}
