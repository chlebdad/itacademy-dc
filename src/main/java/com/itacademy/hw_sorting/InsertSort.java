package com.itacademy.hw_sorting;

public class InsertSort {

    public static int[] insertionSort(int[] list){

        /*
        i ← 1
            while i < length(A)
             j ← i
                     while j > 0 and A[j-1] > A[j]
       swap A[j] and A[j-1]
        j ← j - 1
    end while
    i ← i + 1
end while

         */
        int temp;
        for (int i=0; i<list.length; i++){
            for(int j=i; j>0;j--){

                if( list[j-1] > list[j]) {
                    temp=list[j];
                    list[j] = list[j-1];
                    list[j-1] = temp;

                }

            }

        }


        return list;
    }

}
