package com.itacademy.hw_hashmap;

import java.security.SecureRandom;
import java.util.*;

public class MainClass {

    private static final Random RANDOM = new SecureRandom();

    public static void main(String[] args) {

        List<Person> employees = new LinkedList<>();
        Person person = new Person("John", 45);
        Person person1 = new Person("John", 45);
        employees.add(person1);
        employees.add(person);


        for (int i = 0; i < 5; i++) {

            Person person3 = new Person(randomName(6), RANDOM.nextInt(80));
            employees.add(person3);

        }

        Set<Person> uniqueEmployees = new HashSet<>(employees);
        System.out.println("uniqueEmployees (HashSet) size is: " + uniqueEmployees.size());
        System.out.println("Set contains: ");
        printSet(uniqueEmployees);

        Map<String, Person> nameToPerson = new HashMap<>();

        for (int i = 0; i < employees.size(); i++) {

            Person empl = employees.get(i);
            nameToPerson.put(empl.getName(),empl);

        }

//        for (Person emplNumber: employees) {
//
//            Person empl = employees.get(i);
//            nameToPerson.put(empl.getName(),empl);
//
//        }

        System.out.println("uniqueEmployees (HashMap) size is: " + nameToPerson.size());
        System.out.println("Map contains: ");
        printMap(nameToPerson);

    }

    private static String randomName(int length) {

        final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuilder returnValue = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }
        return new String(returnValue);
    }


    private static void printSet(Set set){

        if(!set.isEmpty()) {
            for (Object setElements : set) {
                System.out.println(setElements);
            }
        } else {
            System.out.println("Set is empty!");
        }
    }

    private static void printMap(Map<String, Person> map){

        if(!map.isEmpty()) {
            for (Map.Entry<String, Person> mapElements : map.entrySet()) {

                System.out.println("Key: " + mapElements.getKey() + " has value of: "+ mapElements.getValue());

            }
        } else {
            System.out.println("Map is empty!");
        }
    }


}
