package com.itacademy.hw_arrays_addition;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        System.out.println(Arrays.toString(Main.xyz(null, null)));
        System.out.println(Arrays.toString(Main.xyz(new int[]{1, 2, 3}, null)));
        System.out.println(Arrays.toString(Main.xyz(null, new int[]{1, 2, 3})));
        System.out.println(Arrays.toString(Main.xyz(new int[]{}, new int[]{})));
        System.out.println(Arrays.toString(Main.xyz(new int[]{7, 7}, new int[]{})));
        System.out.println(Arrays.toString(Main.xyz(new int[]{}, new int[]{7, 7})));
        System.out.println(Arrays.toString(Main.xyz(new int[]{2, 3}, new int[]{4, 4})));
        System.out.println(Arrays.toString(Main.xyz(new int[]{6, 6}, new int[]{4, 4})));
        System.out.println(Arrays.toString(Main.xyz(new int[]{1, 2, 3}, new int[]{4, 4})));
        System.out.println(Arrays.toString(Main.xyz(new int[]{9, 2, 3}, new int[]{8, 4})));
        System.out.println(Arrays.toString(Main.xyz(new int[]{2, 3}, new int[]{1, 4, 4})));
        System.out.println(Arrays.toString(Main.xyz(new int[]{2, 3}, new int[]{9, 8, 4})));


    }


    public static int[] xyz(int[] listOne, int[] listTwo) {


        int[] result;
        int[] bigger;
        int[] smaller;
        int extraOne = 0;

        if (listOne == null && listTwo == null) {
            System.out.println("Arrays contain null values");
            return new int[]{};
        } else if (listOne == null) {
            return listTwo;
        } else if (listTwo == null) {
            return listOne;
        }

        if (listOne.length == 0 && listTwo.length == 0) {
            System.out.println("Both arrays are empty");
            return new int[]{};
        }
        if (listOne.length == 0) {
            return listTwo;
        }
        if (listTwo.length == 0) {
            return listOne;
        }

        if (listOne.length >= listTwo.length) {
            bigger = listOne;
            smaller = listTwo;
        } else {
            smaller = listOne;
            bigger = listTwo;
        }

        int j = 0;
        result = new int[bigger.length + 1];

        for (int i = bigger.length; i >= 1; i--) {

            j++;
            if (smaller.length >= j) {
                result[i] = bigger[i - 1] + smaller[smaller.length - j] + extraOne;
                if (result[i] > 9) {
                    extraOne = 1;
                    result[i] = result[i] - 10;
                } else {
                    extraOne = 0;
                }
            } else if (i < smaller.length) {

                result[i] = bigger[i - 1] + extraOne;
                if (result[i] > 9) {
                    extraOne = 1;
                    if (result[i] >= 10) {
                        result[i - 1] = extraOne;
                    }
                    result[i] = result[i] - 10;
                } else {
                    extraOne = 0;
                }
            } else {
                result[i - 1] = bigger[i - 1];
            }

        }

        return result;

    }
}
