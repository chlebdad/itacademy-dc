package com.itacademy.hw_arrays_addition;

import java.util.Arrays;

public class Maciek_solution {


    public static void main(String[] args) {

//        System.out.println(Arrays.toString(Maciek_solution.addArrays(null, null)));
//        System.out.println(Arrays.toString(Maciek_solution.addArrays(new int[]{1, 2, 3}, null)));
//        System.out.println(Arrays.toString(Maciek_solution.addArrays(null, new int[]{1, 2, 3})));
        System.out.println(Arrays.toString(Maciek_solution.addArrays(new int[]{}, new int[]{})));
        System.out.println(Arrays.toString(Maciek_solution.addArrays(new int[]{7, 7}, new int[]{})));
        System.out.println(Arrays.toString(Maciek_solution.addArrays(new int[]{}, new int[]{7, 7})));
        System.out.println(Arrays.toString(Maciek_solution.addArrays(new int[]{2, 3}, new int[]{4, 4})));
        System.out.println(Arrays.toString(Maciek_solution.addArrays(new int[]{6, 6}, new int[]{4, 4})));
        System.out.println(Arrays.toString(Maciek_solution.addArrays(new int[]{1, 2, 3}, new int[]{4, 4})));
        System.out.println(Arrays.toString(Maciek_solution.addArrays(new int[]{9, 2, 3}, new int[]{8, 4})));
        System.out.println(Arrays.toString(Maciek_solution.addArrays(new int[]{2, 3}, new int[]{1, 4, 4})));
        System.out.println(Arrays.toString(Maciek_solution.addArrays(new int[]{2, 3}, new int[]{9, 8, 4})));

    }

    private static int[] addArrays(int[] first, int[] second) {
        // validation
        int[] shorter, longer;
        if (first.length > second.length) {
            shorter = second;
            longer = first;
        } else {
            shorter = first;
            longer = second;
        }
        final int offset = longer.length - shorter.length;
        boolean overflow = false;
        int[] out = new int[longer.length];
        for (int i = longer.length - 1; i >= 0; i--) {
            int sum;
            if (i >= offset) {
                sum = longer[i] + shorter[i - offset];
            } else {
                sum = longer[i];
            }
            if (overflow) {
                sum++;
            }
            if (sum > 9) {
                overflow = true;
                out[i] = sum % 10;
            } else {
                overflow = false;
                out[i] = sum;
            }
        }
        return out;
    }

}
